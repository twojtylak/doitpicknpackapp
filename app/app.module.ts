/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-14
 * @copyright: 2017 solutionDrive GmbH
 */

import {NgModule} from "@angular/core";
import {NativeScriptI18nModule} from "nativescript-i18n/angular";
import {NativeScriptFormsModule} from "nativescript-angular/forms";
import {NativeScriptHttpModule} from "nativescript-angular/http";
import {NativeScriptModule} from "nativescript-angular/platform";
import {NativeScriptRouterModule} from "nativescript-angular/router";
import {AppComponent} from "./app.component";
import {routes, navigatableComponents} from "./app.routing";
import {UserService} from "./shared/user/user.service";
import {PickedItemsPipe} from "./utils/pipes/pickedItemsPipe";
import {UnpickedItemsPipe} from "./utils/pipes/unpickedItemsPipe";
import {ModalDialogService} from "nativescript-angular";
import {ArticleModalComponent} from "./pages/article/article-modal.component";
import {SlidesModule} from 'nativescript-ng2-slides';



@NgModule({
    imports: [
        NativeScriptModule,
        NativeScriptI18nModule,
        NativeScriptFormsModule,
        NativeScriptHttpModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forRoot(routes),
        SlidesModule
    ],
    declarations: [
        AppComponent,
        PickedItemsPipe,
        UnpickedItemsPipe,
        ArticleModalComponent,
        ...navigatableComponents
    ],
    bootstrap: [AppComponent],
    providers: [
        ModalDialogService,
        UserService
    ],
    entryComponents: [ArticleModalComponent]

})
export class AppModule {
}