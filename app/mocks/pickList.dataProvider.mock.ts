/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-15
 * @copyright: 2017 solutionDrive GmbH
 */

import {PickList} from "../shared/pickList/pickList";
import {Article} from "../shared/article/article";
import {PickListItem} from "../shared/pickList/pickListItem";

export class PickListDataProviderMock {


    /**
     * @param numItems
     *
     * @returns {PickList}
     */
    static generateFakePicklist(numItems: number) {

        let pickList = new PickList();

        pickList.id = 100;

        let pickListItems = [];

        for (let i = 0; i < numItems; i++) {

            let article = new Article();
            article.name = i + " / " + this.getRandomArticleName();

            article.sku = this.getArticleNumber(i);

            let articlePictures = [];

            // between 1 and 4 pictures
            let randomNumberOfPics = Math.ceil(Math.random() * 4);

            for (let j = 0; j < randomNumberOfPics; j++) {
                articlePictures.push(this.getRandomArticlePicture());
            }
            article.pictures = articlePictures;

            let pickListItem = new PickListItem();
            pickListItem.article = article;

            pickListItem.amountToPick = this.generateRandomPickAmount();

            // if (i > 5) {
            //     pickListItem.isPicked = true;
            // }

            pickListItems.push(pickListItem);
        }

        pickList.items = pickListItems;
        return pickList;

    }

    /**
     * @returns string
     */
    static getRandomArticlePicture(){

        let pictures = [
            "http://www.craemerco.de/shop/image/productthumb/front/101000010908/1477576592",
            "http://www.craemerco.de/shop/image/category/back/105000001240/1486379629",
            "http://www.craemerco.de/shop/image/category/back/108000017155/1486973111",
            "http://www.craemerco.de/shop/image/category/back/101000011361/1486123609",
            "http://www.craemerco.de/shop/image/category/front/102000010765/1486124014",
            "http://www.craemerco.de/shop/image/category/front/101000011260/1486123953",
            "http://www.craemerco.de/shop/image/category/back/112000009158/1485778684"
        ];
        return pictures[Math.floor(Math.random() * pictures.length)];
    }

    /**
     *
     * @returns string
     */
    static getRandomArticleName() {

        let articleNames = [
            'ATF MILFORD',
            'Imperial Pullover',
            'Calvin Klein blah blah',
            'KNOWLEDGE COTTON APPAREL  Heavy Twill Chino Peacoat 29/32'
        ];

        return articleNames[Math.floor(Math.random() * articleNames.length)];
    }

    /**
     *
     * @returns {number}
     */
    static generateRandomPickAmount() {
        return Math.ceil(Math.random() * 4);
    }

    /**
     * @param i
     * @returns {string}
     */
    static getArticleNumber(i) {

        // one real, all others fake..
        let number = 1201000011235011;

        if (i > 0) {
            number += i;
        }
        return number.toString();
    }
}