/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-16
 * @copyright: 2017 solutionDrive GmbH
 */

export class Barcode {

    constructor(public code: string, public type : string) {
    }

}