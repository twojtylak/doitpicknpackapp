/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-16
 * @copyright: 2017 solutionDrive GmbH
 */
import {BarcodeScanner} from "../barcodeScanner";
import {Barcode} from "../barcode";

export class IosBarcodeScanner implements BarcodeScanner {

    /**
     *
     */
    protected mtbBarcodeScanner: MTBBarcodeScanner;

    /**
     *
     */
    protected supportedCodeTypes: Array<string>;

    /**
     *
     */
    protected foundCodeCallback: (barcode: Barcode) => void;


    constructor() {

        if (!MTBBarcodeScanner.cameraIsPresent()) {
            throw new Error('Camera ist not available');
        }

        this.mtbBarcodeScanner = MTBBarcodeScanner.alloc();

        this.supportedCodeTypes = [AVMetadataObjectTypeUPCECode,
            AVMetadataObjectTypeCode39Code,
            AVMetadataObjectTypeCode39Mod43Code,
            AVMetadataObjectTypeCode93Code,
            AVMetadataObjectTypeCode128Code,
            AVMetadataObjectTypeEAN8Code,
            AVMetadataObjectTypeEAN13Code,
            AVMetadataObjectTypeAztecCode,
            AVMetadataObjectTypePDF417Code,
            AVMetadataObjectTypeInterleaved2of5Code,
            AVMetadataObjectTypeITF14Code,
            AVMetadataObjectTypeQRCode
        ];
    }

    getView() {

        let view = UIView.alloc().initWithFrame({origin: {x: 0, y: 0}, size: {width: 400, height: 125}});
        this.mtbBarcodeScanner = new MTBBarcodeScanner({
            metadataObjectTypes: this.supportedCodeTypes,
            previewView: view
        });

        return view;
    }

    /**
     * @param turnOn
     */
    setFlashLight(turnOn: boolean) {
        this.mtbBarcodeScanner.toggleTorch();
        // this.mtbBarcodeScanner.torchMode = MTBTorchMode.On;
    }

    startScanning() : void {

        this.mtbBarcodeScanner.startScanningWithResultBlockError((codes: NSArray) => {

            try {
                let barcodeObject = codes.firstObject;

                if (barcodeObject.stringValue) {

                    let barcode = new Barcode(barcodeObject.stringValue, barcodeObject.type);
                    this.foundCodeCallback(barcode);
                }
            } catch (e) {
                // console.log('catched exception' + e.message);
            }

        }, null);
    }

    stopScanning() {
        this.mtbBarcodeScanner.stopScanning();
    }

    setSupportedCodes() {
    }

    /**
     * @param callback
     */
    onFoundBarcode(callback: (barcode: Barcode) => any) : void {
        this.foundCodeCallback = callback;
    }
}