/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-16
 * @copyright: 2017 solutionDrive GmbH
 */
import {Barcode} from "./barcode";

export interface BarcodeScanner {

    getView() : UIView;

    setFlashLight(turnOn : boolean);

    startScanning();

    stopScanning();

    setSupportedCodes();

    onFoundBarcode(callback: (barcode: Barcode) => any);
}