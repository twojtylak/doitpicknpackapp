/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-16
 * @copyright: 2017 solutionDrive GmbH
 */


import {PipeTransform, Pipe} from "@angular/core";
import {PickListItem} from "../../shared/pickList/pickListItem";

@Pipe({name: 'unpickedItems', pure: false})
export class UnpickedItemsPipe implements PipeTransform {

    /**
     * @param allItems
     * @returns {PickListItem[]}
     */
    transform(allItems: PickListItem[]) {
        return allItems.filter(item => !item.isPicked);
    }
}
