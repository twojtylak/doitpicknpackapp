/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-16
 * @copyright: 2017 solutionDrive GmbH
 */


import {PipeTransform, Pipe} from "@angular/core";
import {PickListItem} from "../../shared/pickList/pickListItem";

@Pipe({name: 'pickedItems', pure: false})
export class PickedItemsPipe implements PipeTransform {

    /**
     * @param allItems
     * @returns {PickListItem[]}
     */
    transform(allItems: PickListItem[]) {

        let pickedItems = allItems.filter(item => item.isPicked);
        pickedItems.sort((a: PickListItem, b: PickListItem) => {
            return a.pickingDate > b.pickingDate
                ? -1
                : a.pickingDate < b.pickingDate ? 1 : 0;
        });
        return pickedItems;
    }
}
