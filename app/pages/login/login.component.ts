/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-14
 * @copyright: 2017 solutionDrive GmbH
 */

import {Component, OnInit, ViewChild, ElementRef} from "@angular/core";

import { User } from "../../shared/user/user";

import {UserService} from "../../shared/user/user.service";
import {Router} from "@angular/router";
import {Page} from "ui/page";
import {Color} from "color";

@Component({
    selector: "my-app",
    templateUrl: "pages/login/login.html",
    providers: [UserService],
    styleUrls: ["pages/login/login-common.css", "pages/login/login.css"]
})
export class LoginComponent implements OnInit{

    user: User;

    @ViewChild("container") container: ElementRef;

    constructor(private router: Router, private userService: UserService, private page: Page) {
        this.user = new User();
        this.user.email = "admin";
        this.user.password = "admin"
    }
    ngOnInit(): void {
        this.page.actionBarHidden = true;
        // this.page.backgroundImage = "res://dotspattern3.png";
        this.page.backgroundColor =  new Color("252D38");
    }

    submit() {

        this.router.navigate(["/start"]);
        //todo: uncomment login check
        // this.login();
    }

    login() {
        this.userService.login(this.user)
            .subscribe(
                () => this.router.navigate(["/start"]),
                (error) => alert("Unfortunately we could not find your account.")
            );

    }
}