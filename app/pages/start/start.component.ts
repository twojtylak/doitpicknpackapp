/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-14
 * @copyright: 2017 solutionDrive GmbH
 */

import { Component } from "@angular/core";
import {Router} from "@angular/router";
import * as dialogs from "ui/dialogs";
import {Config} from "../../shared/config";
import {PickListService} from "../../shared/pickList/pickList.service";

@Component({
    selector: "start",
    templateUrl: "pages/start/start.html",
    styleUrls: ["pages/start/start-common.css", "pages/start/start.css"],
    providers: [PickListService]
})
export class StartComponent {

    constructor(private router: Router, private pickListService: PickListService){

    }

    logout() {
        this.router.navigate(["logout"]);
    }

    startPicking() {

       if (this.pickListService.hasActivePicklist()) {
           this.router.navigate(["pick"]);
       } else {
           this.showPickingDialog(0, Config.pickListCreateFormElements, [])();
       }
    }

    /**
     * @param formElementKey
     * @param formElements
     * @param formData
     */
    showPickingDialog(formElementKey, formElements, formData) {

        let self = this;

        if (!formElements[formElementKey]) {
            return function() {
                self.createPickList(formData);
            }
        } else {

            let formElement = formElements[formElementKey];

            let options = {
                title: formElement.headline,
                cancelButtonText: "Cancel",
                actions: formElement.options
            };

            return function() {

                dialogs.action(options).then((result) => {

                    // console.log(result);

                    if (result && result != options.cancelButtonText) {
                        formData[formElement.key] = result;
                        let nextCallback = self.showPickingDialog(formElementKey + 1, formElements, formData);
                        nextCallback();
                    }
                });
            };
        }
    }

    /**
     * @param data
     */
    createPickList(data) {
        Config.currentPicklist =  this.pickListService.getNewPicklist(data);
        this.router.navigate(["pick"]);
    }
}
