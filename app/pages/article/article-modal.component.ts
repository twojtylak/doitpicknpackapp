/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-14
 * @copyright: 2017 solutionDrive GmbH
 */

import {Component, OnInit} from "@angular/core";
import {Article} from "../../shared/article/article";
import {ModalDialogParams} from "nativescript-angular";

@Component({
    selector: "article-modal",
    templateUrl: "pages/article/article-modal.html",
    styleUrls: ["pages/article/article-modal-common.css"]
})
export class ArticleModalComponent implements OnInit {

    protected article: Article;

    /**
     * @param params
     */
    constructor(private params: ModalDialogParams) {
        this.article = params.context;
    }

    ngOnInit(): void {
    }

    /**
     *
     */
    closeModal() {
        this.params.closeCallback();
    }
}
