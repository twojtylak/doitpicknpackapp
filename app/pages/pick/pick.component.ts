/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-14
 * @copyright: 2017 solutionDrive GmbH
 */

import {Component, OnInit, ChangeDetectionStrategy, ViewContainerRef, ChangeDetectorRef, NgZone} from "@angular/core";
import {PickListService} from "../../shared/pickList/pickList.service";
import {PickList} from "../../shared/pickList/pickList";
import {PickListItem} from "../../shared/pickList/pickListItem";
import * as dialogs from "ui/dialogs";
import {Router} from "@angular/router";
import {PickedItemsPipe} from "../../utils/pipes/pickedItemsPipe";
import {Article} from "../../shared/article/article";
import {ModalDialogOptions, ModalDialogService} from "nativescript-angular";
import {ArticleModalComponent} from "../article/article-modal.component";
import {BarcodeScanner} from "../../utils/barcodeScanner/barcodeScanner";
import {IosBarcodeScanner} from "../../utils/barcodeScanner/ios/iosBarcodeScanner";
import {TNSPlayer} from "nativescript-audio";

@Component({
    selector: "pick",
    templateUrl: "pages/pick/pick.html",
    styleUrls: ["pages/pick/pick-common.css", "pages/pick/pick.css"],
    providers: [PickListService],
    changeDetection: ChangeDetectionStrategy.Default
})
export class PickComponent implements OnInit {

    public init: boolean = false;

    public tabindex: number;

    protected pickList: PickList;

    public scanner: BarcodeScanner;

    public lastFoundBarcode: string = '';

    protected player: TNSPlayer;

    /**
     * @param pickListService
     * @param router
     * @param modalService
     * @param vcRef
     * @param zone
     */
    constructor(private pickListService: PickListService, private router: Router, private modalService: ModalDialogService,
                private vcRef: ViewContainerRef, public zone: NgZone) {
    }

    /**
     *
     */
    ngOnInit() {

        // console.log('ngOnInit..');
        this.init = true;
        this.tabindex = 1;
        this.pickList = this.pickListService.getCurrentPickList();
        this.player = new TNSPlayer();
    }

    /**
     * @param args
     */
    creatingView(args) {

        let self = this;

         if (!this.scanner){
            // console.log('try to init camera....');
            try {

                this.scanner = new IosBarcodeScanner();
                args.view = this.scanner.getView();

                this.scanner.onFoundBarcode(barcode => {

                    // console.log('on found barcode...');
                    // only handle on barcode at a time... (workaround for multiple fired events)
                    if (self.lastFoundBarcode != barcode.code) {

                        self.lastFoundBarcode = barcode.code;

                        let foundItems = self.pickList.getItemsByBarcode(barcode.code);
                        // console.log(barcode.code + 'Found items' + foundItems.length);

                        if (foundItems.length == 1 && !foundItems[0].isPicked) {

                            if (!this.player.isAudioPlaying()) {
                                this.player.playFromFile({audioFile: "~/sounds/beep-06.mp3", loop: false});
                            }
                            // necessary for updating list
                            self.zone.run(() => {
                                self.pickByScanner(foundItems[0]);
                            });
                            // console.log('set is processing barcode to false');

                        } else {

                            if (!this.player.isAudioPlaying()) {
                                this.player.playFromFile({audioFile: "~/sounds/beep-negativ.mp3", loop: false, });
                            }
                        }
                    }

                });
                this.scanner.startScanning();

            } catch (e) {
                // console.log(e.message);
            }
         }
    }

    /**
     * @param item
     */
    pickByScanner(item: PickListItem) {

        // console.log('Number of items to pick' + item.amountToPick );
        // if only one item: No Additional dialog necessary
        if (item.amountToPick == 1) {
            item.setIsPicked(true);
            item.amountPicked = 1;
        } else {
            // console.log('Selection necessary');
            this.selectNumberOfItemsToPick(item);
        }
    }

    /**
     * @param item
     */
    pickManual(item: PickListItem) {

        if (item.amountToPick > 1) {
            this.selectNumberOfItemsToPick(item);
        } else {
            dialogs.confirm(L('CONFIRM_MANUAL_PICKING', item.article.name, item.amountToPick)).then(result => {
                if (result) {
                    item.setIsPicked(true);
                }
            });
        }
    }

    /**
     * @param item
     */
    selectNumberOfItemsToPick(item: PickListItem) {

        let numOfItemsActions = [];

        for (let i = 1; i <= item.amountToPick; i ++) {
            numOfItemsActions.push(i.toString());
        }

        let options = {
            title: item.article.name,
            message: L('SELECT_NUM_OF_ITEMS_TO_PICK'),
            cancelButtonText: "Cancel",
            actions: numOfItemsActions
        };

        dialogs.action(options).then((result) => {

            if (parseInt(result) > 0) {
                // console.log('set Num of items '+result);
                item.amountPicked = parseInt(result);
                item.setIsPicked(true);
            }
        });
    }


    /**
     * @param item
     */
    markAsUnpicked(item: PickListItem){

        dialogs.confirm(L('CONFIRM_MARK_AS_UNPICKED', item.article.name, item.amountToPick)).then(result => {
            if (result) {
                item.setIsPicked(false);
            }
        });
    }

    /**
     *
     */
    finishPicking() {

        let pickedItems = new PickedItemsPipe().transform(this.pickList.items);
        let numOfItems = this.pickList.items.length;

        dialogs.confirm(L('CONFIRM_PARTIAL_PICKING_FINISHED', pickedItems.length, numOfItems)).then(result => {

            if (result) {
                // todo save picking via service => relocate to start
                this.pickListService.finishPicklist();
                this.router.navigate(["/start"]);
            }
        });
    }

    /**
     *
     * @param article
     */
    createArticleModal(article: Article) {

        let options : ModalDialogOptions = {
            context: article,
            viewContainerRef: this.vcRef
        };
        this.modalService.showModal(ArticleModalComponent, options);
    }

    /**
     */
    toggleFlash() {
        try {
            this.scanner.setFlashLight(false);
        } catch(e) {

        }
    }

    startScanning() {
        // console.log('Scanner start...');

        this.scanner.startScanning();
    }

    stopScanning() {
        // console.log('Scanner stop...');
        this.scanner.stopScanning();
    }


    testFirstElement() {
        this.player.playFromFile({audioFile: "~/sounds/beep-06.mp3", loop: false});
    }

    testRefresh() {
        // console.log('nothing to do');
    }
}
