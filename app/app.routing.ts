/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-14
 * @copyright: 2017 solutionDrive GmbH
 */

import { LoginComponent } from "./pages/login/login.component";
import {PickComponent} from "./pages/pick/pick.component";
import {StartComponent} from "./pages/start/start.component";
import {PicklistCreateComponent} from "./pages/pick/picklist-create/picklist-create";

export const routes = [
    // { path: "", component: LoginComponent },
    { path: "", component: StartComponent},
    { path: "start", component: StartComponent},
    { path: "pick", component: PickComponent },
    { path: "logout", redirectTo: ""},
];

export const navigatableComponents = [
    LoginComponent,
    StartComponent,
    PickComponent
];