/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-14
 * @copyright: 2017 solutionDrive GmbH
 */

export class Config {
  static apiUrl = "http://192.168.190.79/";
  static clientId = "1_3bcbxd9e24g0gk4swg0kwgcwg4o8k8g4g888kwc44gcc0gwwk4";
  static clientSecret = "4ok2x70rlfokc8g0wws8c8kwcokw80k44sg48goc0ok4w0so0k";
  static token = "";

  static currentPicklist = null;
  static pickListCreateFormElements = [
      { headline: 'Anzahl Items',
        key: 'numOfItems',
        options: ["10", "25", "50", "100", "200", "500"]
      },
      {
        headline: 'Type',
        key: 'type',
        options: [ "Please (Lager)", "Fläche (Damen)", "Fläche (Herren)", "Problemfälle"]
      }
  ];
}