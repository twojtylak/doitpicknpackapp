/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-14
 * @copyright: 2017 solutionDrive GmbH
 */

import {PickListItem} from "./pickListItem";

export class PickList {

    id: number;
    status: number;
    items: PickListItem[];

    /**
     * @param {string} barcode
     * @returns {PickListItem[]}
     */
    getItemsByBarcode(barcode: string) {
        return this.items.filter(item  => item.article.sku == barcode);
    }
}