/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-14
 * @copyright: 2017 solutionDrive GmbH
 */

import {Article} from "../article/article";

export class PickListItem {

    amountToPick: number;
    amountPicked: number;
    amountNotFound: number;
    article: Article;
    isPicked = false;
    pickingDate = null;

    /**
     * @param picked
     */
    setIsPicked(picked: boolean) {
       this.isPicked = picked;
       if (picked) {
         this.pickingDate = Date.now();
       } else {
           this.pickingDate = null;
       }
    }
}