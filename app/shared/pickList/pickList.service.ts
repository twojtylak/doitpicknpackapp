/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-14
 * @copyright: 2017 solutionDrive GmbH
 */

import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {PickList} from "./pickList";
import {PickListItem} from "./pickListItem";
import {Article} from "../article/article";
import {PickListDataProviderMock} from "../../mocks/pickList.dataProvider.mock";
import {Config} from "../config";

@Injectable()
export class PickListService {

    constructor(private http: Http) {}

    /**
     * @todo get this data from api
     *
     *
     * @returns {PickList}
     */
    getNewPicklist(data) {

        return PickListDataProviderMock.generateFakePicklist(data.numOfItems);
    }

    /**
     * @returns {boolean}
     */
    hasActivePicklist() {
        return Config.currentPicklist != null;
    }

    /**
     *
     * @returns {PickList}
     */
    getCurrentPickList() {

        let list = Config.currentPicklist;
        return list;
    }


    finishPicklist() {
        // todo api calls...
        Config.currentPicklist = null;
    }
}