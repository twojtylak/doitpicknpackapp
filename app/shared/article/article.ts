/*
 * Created by solutionDrive GmbH.
 *
 * @author: Tobias Wojtylak <tw@solutiondrive.de>
 * @date: 2017-02-14
 * @copyright: 2017 solutionDrive GmbH
 */

export class Article {

    id: string;
    sku: string;
    name : string;
    attributes: any;
    pictures: Array<string> = [];

    /**
     * @returns {string}
     */
    getListPicture() {

        if (this.pictures.length > 0) {
            return this.pictures[0];
        }
        return '';
    }

    /**
     * @returns {Array<string>}
     */
    getDetailPictures() {
        return this.pictures;
    }
}