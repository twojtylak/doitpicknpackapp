# NativeScript Pick`nPack App

##TODO

Remove following code from app/App_Resources/iOS/Info.plist
 (Necessary for oauth Login without https)
```xml
<key>NSAppTransportSecurity</key>
<dict>
    <!--Include to allow all connections (DANGER)-->
    <key>NSAllowsArbitraryLoads</key>
    <true/>
</dict>
```

Adjusted code in node_modules/nativescript-barcodescanner/barcodescanner.ios.js:128
```javascript
    else if (format === "2OF5")
        types.push(AVMetadataObjectTypeInterleaved2of5Code);
    else if (format === "2D")
        types.push(AVMetadataObjectTypeDataMatrixCode);
    }
}
else {
    types = [AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
            AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
            AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode, AVMetadataObjectTypeInterleaved2of5Code,
            AVMetadataObjectTypeDataMatrixCode];
```
